<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\User;

class UserRegisteredMail extends Mailable
{
    use Queueable, SerializesModels;

    protected $user, $kode;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(User $user, $kode)
    {
        //
        $this->user = $user;
        $this->kode = $kode;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('example@example.com')
                    ->view('send_email_user_registered')
                    ->with([
                        'name' => $this->user->name,
                        'kode' => $this->kode
                    ]);
    }
}
