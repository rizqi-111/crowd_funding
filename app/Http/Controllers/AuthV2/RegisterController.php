<?php

namespace App\Http\Controllers\AuthV2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;
use App\User;
use App\Role;
use App\Otp_code;
use App\Events\UserRegisteredEvent;

class RegisterController extends Controller
{
    //
    public function register(Request $request){
        $validate = $request->validate([
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        ]);

        $otp_code = mt_rand(100000, 999999);
        $valid = Carbon::now('Asia/Jakarta')->addMinutes(5);
        
        $role_id = Role::where('name','=','user')->first()->id;

        $user = User::create([
            'name' => $request->name, 
            'email'=> $request->email, 
            'password' => Hash::make('rahasia'),
            'role_id' => $role_id
        ]);

        $otp = new Otp_code;
        $otp->code = $otp_code;
        $otp->expired_time = $valid;
        
        $user->otp_code()->save($otp);
        
        event(new UserRegisteredEvent($user,$otp_code));

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Silahkan Cek Email',
            'user' => $user
        ]);
    }

    public function update_password(Request $request){
        $validate = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8', 'max:255', 'confirmed']
        ]);

        $user = User::where('email','=', $request->email)->first();
        $user->password = Hash::make($request->password);
        $user->save();

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Berhasil Ubah Password',
            'user' => $user
        ]);
    }

    public function generate_otp(Request $request){
        $validate = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255']
        ]);

        $user = User::where('email','=', $request->email)->first();
        
        if(!$user){
            return response()->json([
                'response_code' => '01',
                'response_msg' => ' Email Tidak Ditemukan'
            ]);
        }

        $otp_code = mt_rand(100000, 999999);
        $valid = Carbon::now('Asia/Jakarta')->addMinutes(5);

        $otp = Otp_code::where('user_id',$user->id)
        ->update([
           'code' => $otp_code,
           'expired_time' => $valid
         ]);
        
        return response()->json([
            'response_code' => '00',
            'response_msg' => ' Berhasil Generate OTP Code, Silahkan Cek Email',
            'user' => $user
        ]);
    }
}
