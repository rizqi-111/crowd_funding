<?php

namespace App\Http\Controllers\AuthV2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    //
    public function login(Request $request){
        $validate = $request->validate([
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:6', 'max:255']
        ]);

        $credentials = request(['email', 'password']);
        if (! $token = auth()->attempt($credentials)) {
            return response()->json(['error' => 'Password Anda Salah'], 401);
        }

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'User Berhasil Login',
            'token' => $token,
            'user' => Auth::user()
        ]);
    }
}
