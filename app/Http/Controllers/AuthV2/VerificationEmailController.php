<?php

namespace App\Http\Controllers\AuthV2;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Carbon\Carbon;
use App\Otp_code;

class VerificationEmailController extends Controller
{
    //
    public function verification(Request $request){
        $validate = $request->validate([
            'otp_code' => ['required']
        ]);

        $otp = Otp_code::where('code','=',$request->otp_code)->first();
        
        if($otp == null){ //jika otp_code tidak ditemukan
            return response()->json([
                'response_code' => '01',
                'response_msg' => 'OTP Code Tidak Ditemukan'
            ]);
        } else {
            if(Carbon::now('Asia/Jakarta')->lte($otp->expired_time)){
                $user = $otp->user;
                $user->email_verified_at = Carbon::now('Asia/Jakarta');
                $user->save();
                return response()->json([
                    'response_code' => '00',
                    'response_msg' => 'OTP Code Berhasil Diverifkasi',
                    'user' => $otp->user
                ]);
            } else {
                return response()->json([
                    'response_code' => '01',
                    'response_msg' => 'OTP Code Sudah Tidak Berlaku, Silahkan Generate Ulang'
                ]);
            }
        }
    }
}
