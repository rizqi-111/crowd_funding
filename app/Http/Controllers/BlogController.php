<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Blog;

class BlogController extends Controller
{
    //
    public function random($count){
        $blog = Blog::select('*')
                    ->inRandomOrder()
                    ->limit($count)
                    ->get();

        $data['blogs'] = $blog;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Data Blog Berhasil Ditampilkan',
            'data' => $data
        ],200);
    }

    public function store(Request $request){
        $validate = $request->validate([
            'title' => 'required|min:4',
            'content' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);

        $blog = Blog::create([
            'title' => $request->title,
            'content' => $request->content
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $blog->id.".".$image_extension;
            $image_folder = 'blog/';
            $image_location = $image_folder.$image_name;

            try{
                $image->move(public_path($image_folder),$image_name);

                $blog->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e){
                return response()->json([
                    'response_code' => '01',
                    'response_msg' => 'Foto Blog Gagal Upload',
                    'data' => $blog
                ],200);
            }
        };

        $data['blogs'] = $blog;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Data Blog Berhasil Ditambahkan',
            'data' => $data
        ],200);
    }
}
