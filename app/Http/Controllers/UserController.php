<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;

class UserController extends Controller
{
    //
    public function __construct(){
        $this->middleware('auth:api');
    }

    public function get_profile(){
        return response()->json([
            'response_code' => '00',
            'response_msg' => 'User Profile Berhasil Ditampilkan',
            'user' => Auth::user()
        ]);
    }

    public function update(Request $request){
        $validate = $request->validate([
            'name' => 'required|min:4',
            'photo' => 'required'
        ]);

        $user = Auth::user();

        if($user->photo != null){
            Storage::delete($user->photo);
        }

        $path = $request->file('photo')->storeAs(
            'photo', $request->user()->id.'.'.$request->photo->getClientOriginalExtension()
        );

        $user->name = $request->name;
        $user->photo = $path;
        $user->save();

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'User Profile Berhasil Diperbarui',
            'user' => Auth::user()
        ]);
    }

    public function logout()
    {
        auth()->logout();

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'User Profile Berhasil Logout'
        ],200);
    }
}
