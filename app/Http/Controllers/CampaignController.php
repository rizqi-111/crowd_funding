<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Campaign;

class CampaignController extends Controller
{
    //
    public function index(){
        $campaign = Campaign::paginate(6);

        $data['campaigns'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Data Campaign Berhasil Ditampilkan',
            'data' => $data
        ],200);
    }

    public function random($count){
        $campaign = Campaign::select('*')
                    ->inRandomOrder()
                    ->limit($count)
                    ->get();

        $data['campaigns'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Data Campaign Berhasil Ditampilkan',
            'data' => $data
        ],200);
    }

    public function store(Request $request){
        $validate = $request->validate([
            'title' => 'required|min:4',
            'description' => 'required',
            'image' => 'required|mimes:jpg,jpeg,png'
        ]);

        $campaign = Campaign::create([
            'title' => $request->title,
            'description' => $request->description
        ]);

        if($request->hasFile('image')){
            $image = $request->file('image');
            $image_extension = $image->getClientOriginalExtension();
            $image_name = $campaign->id.".".$image_extension;
            $image_folder = 'campaign/';
            $image_location = $image_folder.$image_name;

            try{
                $image->move(public_path($image_folder),$image_name);

                $campaign->update([
                    'image' => $image_location
                ]);
            } catch (\Exception $e){
                return response()->json([
                    'response_code' => '01',
                    'response_msg' => 'Foto Campaign Gagal Upload',
                    'data' => $campaign
                ],200);
            }
        };

        $data['campaigns'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Data Campaign Berhasil Ditambahkan',
            'data' => $data
        ],200);
    }

    public function detail($id){
        $campaign = Campaign::find($id);

        $data['campaign'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Data Campaign Berhasil Ditampilkan',
            'data' => $data
        ],200);
    }

    public function search($keyword){
        $campaign = Campaign::select('*')
                    ->where('title','LIKE','%'.$keyword.'%')
                    ->get();

        $data['campaign'] = $campaign;

        return response()->json([
            'response_code' => '00',
            'response_msg' => 'Data Campaign Berhasil Ditampilkan',
            'data' => $data
        ],200);
    }
}
