<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Laravel\Socialite\Facades\Socialite;
use App\User;
use App\Role;
use Illuminate\Support\Facades\Hash;
use Carbon\Carbon;

class SocialiteController extends Controller
{
    //
    public function redirectToProvider($provider){
        $url = Socialite::driver($provider)->stateless()->redirect()->getTargetUrl();
        
        return response()->json([
            'url' => $url
        ]);
    }

    public function handleProviderCallback($provider){
         try{

            $social_user = Socialite::driver($provider)->stateless()->user();
            
            if(!$social_user){
                return response()->json([
                    'response_code' => '01',
                    'response_msg' => 'login failed,social user'
                ],401);
            }

            $user = User::whereEmail($social_user->email)->first();
            
            if(!$user){
                if($provider == 'google'){
                    $photo_profile = $social_user->avatar;
                }
                
                $role_id = Role::where('name','=','user')->first()->id;

                $user = User::create([
                    'name' => $social_user->name, 
                    'email'=> $social_user->email, 
                    'password' => Hash::make('rahasia'),
                    'email_verified_at' => Carbon::now(),
                    'photo' => $photo_profile,
                    'role_id' => $role_id
                ]);
            }
            
            $data['user'] = $user;
            $data['token'] = auth()->login($user);
            
            return response()->json([
                'response_code' => '00',
                'response_msg' => 'User Berhasil Login',
                'token' => $data['token'],
                'user' => $user
            ]);
         } catch (\Throwable $th) {
            return response()->json([
                'response_code' => '01',
                'response_msg' => 'login failed',
                'data' => $th
            ],401);
         }
    }
}
