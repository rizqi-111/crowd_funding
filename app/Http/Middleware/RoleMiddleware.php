<?php

namespace App\Http\Middleware;

use Closure;
use App\Role;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $role = Auth::user()->role_id;
        $role_name = Role::where('id',$role)->first()->name;
        if($role_name === 'admin'){
            return $next($request);
        }
        else {
            abort(403);
        }
    }
}
