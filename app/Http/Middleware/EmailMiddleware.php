<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\User;

class EmailMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // $email = Auth::user()->email_verified_at;
        if(User::where('email','=', $request->email)->first()){
            $email = User::where('email','=', $request->email)->first()->email_verified_at;
        } else {
            return response()->json(['error' => 'Email Anda Tidak Terdaftar'], 401);
        }
        if($email != null){
            return $next($request);
        }
        else {
            return response()->json(['error' => 'Akun Anda Belum Diverifikasi, Silahkan Regenarete OTP Code'], 401);
        }
    }
}
