<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Otp_code extends Model
{
    //
    protected $fillable = ['expired_time','code'];

    /**
    * Get the value indicating whether the IDs are incrementing.
    *
    * @return bool
    */
    public function getIncrementing()
    {
        return false;
    }

    /**
    * Get the auto-incrementing key type.
    *
    * @return string
    */
    public function getKeyType()
    {
        return 'string';
    }

    protected static function boot() {
        parent::boot();

        static::creating(function ($model) {
            if ( ! $model->getKey()) {
                $model->{$model->getKeyName()} = (string) Str::uuid();
            }
        });
    }

    public function user(){
        return $this->belongsTo('App\User');
    }
}
