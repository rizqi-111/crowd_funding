export default{
    namespaced: true,
    state: {
        user : {},
        photo : ''
    },
    mutations: {
        set: (state,payload) => {
            state.user = payload
        },
        setPhoto: (state,payload) => {
            state.photo = payload
        }
    },
    getters: {
        user : state => state.user,
        photo : state => state.photo,
        guest : state => Object.keys(state.user).length === 0
    },
    actions: {
        set: ({commit},payload) => {
            commit('set',payload)
        },
        setPhoto: ({commit},payload) => {
            if(payload.split('/').includes('https:')){
                commit('setPhoto',payload)
            }else {
                //`${state.path}/api/storage/${state.user.user.photo}`
                // let photoName = state.path+'/api/storage/'+payload
                commit('setPhoto',window.location.origin+'/api/storage/'+payload.split('/')[1])
            }
        },
        checkToken: ({commit},payload) => {
            let config = {
                headers : {
                    'Authorization' : 'Bearer ' + payload.token
                },
            }

            axios.post('/api/profile/check-token', {}, config)
            .then((response) => {
                commit('set',payload)
            })
            .catch((error) => {
                commit('set',{})
            })
        }
    }
}