<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
    </head>
    <body>
        <p>Selamat <b>{{$name}},</b></p><br>
        <p>Akun anda telah terdaftar di aplikasi kami. Berikut ini adalah kode OTP Anda : <b>{{$kode}}</b>,
         untuk memverifikasi akun Anda.</p>
         <p>Silahkan buka link INI, untuk memasukkan kode OTP Anda.</p>
        <p>Terima Kasih</p>
    </body>
</html> 