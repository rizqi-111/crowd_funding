<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8"></meta>
    <link rel="stylesheet" href="{{ url('/css/app.css') }}">
</head>
<body>
    <div id="app">
        <app></app>
    </div>
    <script src="{{ url('/js/app.js') }}"></script>
</body>
</html> 