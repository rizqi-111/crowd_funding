<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('/storage/{filename}','GetStoragePhoto');
Route::post('/register','AuthV2\RegisterController@register');
Route::post('/verification','AuthV2\VerificationEmailController@verification');
Route::post('/generate-otp','AuthV2\RegisterController@generate_otp');
Route::post('/update-password','AuthV2\RegisterController@update_password')->middleware('email');
Route::post('/login','AuthV2\LoginController@login')->middleware('email');

Route::get('/social/{provider}','SocialiteController@redirectToProvider');
Route::get('/social/{provider}/callback','SocialiteController@handleProviderCallback');

Route::group([

    'middleware' => 'auth:api',
    'prefix' => 'profile'

], function ($router) {

    Route::get('/get-profile', 'UserController@get_profile');
    Route::post('/update-profile', 'UserController@update');
    Route::post('/logout', 'UserController@logout');
    Route::post('/check-token','AuthV2\CheckTokenController');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'campaign'
], function ($router) {
    Route::get('random/{count}','CampaignController@random');
    Route::post('store','CampaignController@store');
    Route::get('/','CampaignController@index');
    Route::get('/{id}','CampaignController@detail');
    Route::get('/search/{keyword}','CampaignController@search');
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'blog'
], function ($router) {
    Route::get('random/{count}','BlogController@random');
    Route::post('store','BlogController@store');
});