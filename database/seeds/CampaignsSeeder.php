<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class CampaignsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker\Factory::create();
        $image_path = 'public/campaign';

        for($i = 1; $i <= 15; $i++){
            $image_fullpath =  $faker->image($image_path,640,480, null, false);
            $image_img = explode("/",$image_fullpath);
            DB::table('campaigns')->insert([
                'id' => (string) Str::uuid(),
                'title' => 'Kampanye '.$i,
                'description' => 'Kampanye '.$i.' blablabla',
                'image' => 'campaign/'.$image_img[0],
                'address' => 'Jl. BlaBlaBla',
                'required' => 12345678,
                'collected' => 1234567,
                'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
                'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
            ]);
        }

        // $image_fullpath =  $faker->image($image_path,640,480, null, false);
        // $image_img = explode("/",$image_fullpath);
        // DB::table('campaigns')->insert([
        //     'id' => (string) Str::uuid(),
        //     'title' => 'Kampanye 2',
        //     'description' => 'Kampanye 2 blablabla',
        //     'image' => 'campaign/'.$image_img[0],
        //     'address' => 'Jl. BlaBlaBla',
        //     'required' => 12345678,
        //     'collected' => 1234567,
        //     'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // $image_fullpath =  $faker->image($image_path,640,480, null, false);
        // $image_img = explode("/",$image_fullpath);
        // DB::table('campaigns')->insert([
        //     'id' => (string) Str::uuid(),
        //     'title' => 'Kampanye 3',
        //     'description' => 'Kampanye 3 blablabla',
        //     'image' => 'campaign/'.$image_img[0],
        //     'address' => 'Jl. BlaBlaBla',
        //     'required' => 12345678,
        //     'collected' => 1234567,
        //     'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // $image_fullpath =  $faker->image($image_path,640,480, null, false);
        // $image_img = explode("/",$image_fullpath);
        // DB::table('campaigns')->insert([
        //     'id' => (string) Str::uuid(),
        //     'title' => 'Kampanye 4',
        //     'description' => 'Kampanye 4 blablabla',
        //     'image' => 'campaign/'.$image_img[0],
        //     'address' => 'Jl. BlaBlaBla',
        //     'required' => 12345678,
        //     'collected' => 1234567,
        //     'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
        // ]);

        // $image_fullpath =  $faker->image($image_path,640,480, null, false);
        // $image_img = explode("/",$image_fullpath);
        // DB::table('campaigns')->insert([
        //     'id' => (string) Str::uuid(),
        //     'title' => 'Kampanye 5',
        //     'description' => 'Kampanye 5 blablabla',
        //     'image' => 'campaign/'.$image_img[0],
        //     'address' => 'Jl. BlaBlaBla',
        //     'required' => 12345678,
        //     'collected' => 1234567,
        //     'created_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s'),
        //     'updated_at' => Carbon\Carbon::now()->format('Y-m-d H:i:s')
        // ]);
    }
}
