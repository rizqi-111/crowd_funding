<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('roles')->insert([
            'id' => (string) Str::uuid(),
        	'name' => 'user'
        ]);

        DB::table('roles')->insert([
            'id' => (string) Str::uuid(),
        	'name' => 'admin'
        ]);
    }
}
