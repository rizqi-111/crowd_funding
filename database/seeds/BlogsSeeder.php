<?php

use Illuminate\Database\Seeder;

class BlogsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //

        for($i = 1; $i <= 15; $i++){
            DB::table('blogs')->insert([
                'id' => (string) Str::uuid(),
                'title' => 'Blog '.$i,
                'content' => 'Blog '.$i.' blablabla',
                'image' => 'https://source.unsplash.com/random'
            ]);
        }

        // DB::table('blogs')->insert([
        //     'id' => (string) Str::uuid(),
        //     'title' => 'Blog 2',
        //     'content' => 'Blog 2 blablabla',
        //     'image' => 'https://source.unsplash.com/random'
        // ]);
        // DB::table('blogs')->insert([
        //     'id' => (string) Str::uuid(),
        //     'title' => 'Blog 3',
        //     'content' => 'Blog 3 blablabla',
        //     'image' => 'https://source.unsplash.com/random'
        // ]);
        // DB::table('blogs')->insert([
        //     'id' => (string) Str::uuid(),
        //     'title' => 'Blog 4',
        //     'content' => 'Blog 4 blablabla',
        //     'image' => 'https://source.unsplash.com/random'
        // ]);
    }
}
